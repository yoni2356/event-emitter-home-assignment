class EventEmitter {
    topics;

    constructor() {
        this.topics = {};
    }

    initTopic(eventName) {
        this.topics[eventName] = {
            handlers: []
        }
    }

    getTopic(eventName) {
        if (!this.topics[eventName]) {
            this.initTopic(eventName)
        }
        return this.topics[eventName]
    }

    addHandlerToTopic = (topic, handler) => {
        if (!topic?.handlers) return
        topic.handlers.push(handler)
    }

    fireAllHandlers = (topic, ...data) => { 
        if (!topic?.handlers) return
        topic.handlers.forEach(handler => handler(data))
    }

    on(eventName, handler) {
        const topic = this.getTopic(eventName);
        this.addHandlerToTopic(topic, handler)
    }

    trigger(eventName, ...data) {
        const topic = this.getTopic(eventName)
        this.fireAllHandlers(topic, data)
    }
}

module.exports = EventEmitter